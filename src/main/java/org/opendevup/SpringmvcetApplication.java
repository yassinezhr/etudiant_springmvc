package org.opendevup;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.opendevup.dao.EtudiantRepository;
import org.opendevup.entities.Etudiant;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@SpringBootApplication
public class SpringmvcetApplication {

	public static void main(String[] args) throws ParseException {
		SpringApplication.run(SpringmvcetApplication.class, args);
		/*
		ApplicationContext ctx = SpringApplication.run(SpringmvcetApplication.class, args);// si on vx acceder a tt les objets qui sont créer par spring on déclare un objet de ApplicationContext  
		EtudiantRepository etudiantRepository =  ctx.getBean(EtudiantRepository.class); // on récupere le bean 
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		etudiantRepository.save(new Etudiant("Yassine",df.parse("1994-09-21"), "yassinezouhair@gmail.com", "yas.jpge"));
		etudiantRepository.save(new Etudiant("Amine",df.parse("1998-08-16"), "aminezouhair@gmail.com", "ami.jpge"));
		etudiantRepository.save(new Etudiant("kkkkkkk",df.parse("1998-08-16"), "aminezouhair@gmail.com", "ami.jpge"));
       */
		
		/*
		Page<Etudiant> etds = etudiantRepository.findAll(new PageRequest(0, 5)); // je vx  la page 0 et 5 ligne
        etds.forEach(e->System.out.println(e.getNom())); // ça c'est java8
       */
		
		/*
        Page<Etudiant> etds = etudiantRepository.chercherEtudiants("%k%",new PageRequest(0, 5)); // je vx  la page 0 et 5 ligne
        etds.forEach(e->System.out.println(e.getNom())); // ça c'est java8
		*/
	}
}

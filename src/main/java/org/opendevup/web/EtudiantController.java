package org.opendevup.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.opendevup.dao.EtudiantRepository;
import org.opendevup.entities.Etudiant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;


@Controller
@RequestMapping("/Etudiant")
public class EtudiantController {
	@Autowired //pour l'injecter
	private EtudiantRepository etudiantRepository;
	
	@Value("${dir.images}") // injecter une variable
	private String imageDir;
	
	@RequestMapping("/index")
	public String index(Model model,@RequestParam(name="page",defaultValue="0")int p,@RequestParam(name="mc",defaultValue="")String mc){ // p et mn on utilise ds url
		Page<Etudiant> etds = etudiantRepository.chercherEtudiants("%"+mc+"%", new PageRequest(p, 2));
		int[] pages = new int[etds.getTotalPages()];
		for (int i = 0; i < pages.length; i++) {
			pages[i]=i;
		}
		model.addAttribute("pages", pages);
		model.addAttribute("pageEtudiants",etds);
		model.addAttribute("pageCourante",p);
		model.addAttribute("mc",mc);
		
		
	return "etudiants" ;
	}
	
	@RequestMapping(value="/form")
	public String formEtudiant(Model model){ 
	model.addAttribute("etudiant",new Etudiant());
	return "FormEtudiant" ;
	}
	@RequestMapping(value="/saveEtudiant",method=RequestMethod.POST)
	public String save(@Valid Etudiant etd ,BindingResult bindingResult,@RequestParam(name="picture")MultipartFile file) throws IllegalStateException, IOException{ // le champs picture de type file qui se trouve dans le formulaire va etre stocker dans objet de type Multipartfile qui est file
		if(bindingResult.hasErrors()){
			
			return"FormEtudiant";
			}
		/*if(!file.isEmpty()){
			etd.setPhoto(file.getOriginalFilename());}*/
		etudiantRepository.save(etd);
		if(!file.isEmpty()){
			etd.setPhoto(file.getOriginalFilename());
			//file.transferTo(new File(System.getProperty("user.home")+etd.getId())); // metrre 
			file.transferTo(new File(imageDir+etd.getId()));
		}
	
	return "redirect:index" ;
	}
	
	@RequestMapping(value="/getPhoto", produces=MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] getPhoto(Long id) throws Exception{
		File f = new File(imageDir+id);//récuperer la photo
		return IOUtils.toByteArray(new FileInputStream(f));
	}
	
	@RequestMapping(value="/supprimer")
	public String supprimer(Long id){
		etudiantRepository.delete(id);
		return "redirect:index" ;
	}
	
	@RequestMapping(value="/edit")
	public String edit(Long id,Model model){
		Etudiant et = etudiantRepository.getOne(id);
		model.addAttribute("etudiant",et);
		return "EditEtudiant" ;
	}
	
	@RequestMapping(value="/UpdateEtudiant",method=RequestMethod.POST)
	public String update(@Valid Etudiant etd ,BindingResult bindingResult,@RequestParam(name="picture")MultipartFile file) throws IllegalStateException, IOException{ // le champs picture de type file qui se trouve dans le formulaire va etre stocker dans objet de type Multipartfile qui est file
		if(bindingResult.hasErrors()){
			
			return"EditEtudiant";
			}
		/*if(!file.isEmpty()){
			etd.setPhoto(file.getOriginalFilename());}*/
		etudiantRepository.save(etd); //save faite pour ajouter et update si (et) existe il va modifier sinon il va l'ajouter
		if(!file.isEmpty()){
			etd.setPhoto(file.getOriginalFilename());
			//file.transferTo(new File(System.getProperty("user.home")+etd.getId())); // metrre 
			file.transferTo(new File(imageDir+etd.getId()));
		}
	
	return "redirect:index" ;
	}
	
	
}
